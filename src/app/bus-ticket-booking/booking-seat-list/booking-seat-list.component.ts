import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-booking-seat-list',
  templateUrl: './booking-seat-list.component.html',
  styleUrls: ['./booking-seat-list.component.scss'],
})
export class BookingSeatListComponent implements OnInit {
  @Input() bookingSeatList!: Seat[];
  @Output() eventSetBookingSeat = new EventEmitter();

  constructor() {}

  ngOnInit(): void {}

  setBookingSeat(seat: Seat) {
    this.eventSetBookingSeat.emit(seat);
  }

  calcTotalAmount() {
    return this.bookingSeatList.reduce((totalAmount, seat) => {
      return (totalAmount += seat.Gia);
    }, 0);
  }
}

interface Seat {
  SoGhe: number;
  TenGhe: string;
  Gia: number;
  TrangThai: boolean;
}
